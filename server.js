require('./config/config')
const express = require('express');
const mongoose = require('mongoose');
const app = express();
const routesIndex = require('./routes/routesIndex');
const routesUser = require('./routes/routesUser');
const routesBlog = require('./routes/routesNotes');
const methodOverride = require('method-override');
const session = require('express-session');
const passport = require('passport');
const path = require('path');
require('./config/passport');

app.use(express.json());
app.use(express.urlencoded());
//Cambiar base de datos por la Uds
mongoose.connect(process.env.URLDB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  }, (err, des) =>{
    if(err) throw err;
    console.log('Base de datos online')
});

app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine', 'pug');
app.use(methodOverride('_method'));
app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use((req, res, next)=>{
  res.locals.user = req.user || null;
  next();
});
app.listen(process.env.PORT, () => console.log(`Escuchando puerto ${process.env.PORT}`));
//Primer ruta para la pag
app.use(routesIndex);
//Segunda ruta para ver si se hace un login
app.use(routesUser);
//Ruta para controlar el crud del blog
app.use(routesBlog);

app.use(function(req, res, next) {
    res.status(404).render('404');
});