const express = require('express');
const router = express.Router();
const Noticias = require('../models/noticias');
const Contacto = require('../models/contacto');

router.get("/", async function (req, res) {
    const noticias = await Noticias.find({}).sort({'-created_at': -1}).limit(3);
    res.render('index', { noticias })
});
router.get("/about", function (req, res) {
    res.render('about')
});
router.get("/contact", function (req, res) {
    res.render('contact')
});

module.exports = router